import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AccountUpdateComponent = class AccountUpdateComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
AccountUpdateComponent = __decorate([
    Component({
        selector: 'app-account-update',
        templateUrl: './account-update.component.html',
        styleUrls: ['./account-update.component.css']
    })
], AccountUpdateComponent);
export { AccountUpdateComponent };
//# sourceMappingURL=account-update.component.js.map