import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { AccountUpdateComponent } from './account-update/account-update.component';
let AccountComponent = class AccountComponent {
    constructor(dialog) {
        this.dialog = dialog;
        this.fund_image = "";
        this.accounts_data = [
            {
                name: "Pavel Korchakin",
                date: "1/1/2018",
                email: "me@me.com",
                funds: 5254.5
            },
            {
                name: "Pavel Korchakin",
                date: "1/1/2018",
                email: "me@me.com",
                funds: 325.25
            },
        ];
        this.accounts = [];
    }
    ngOnInit() {
        this.accounts = this.accounts_data;
    }
    onUploadFundImage() {
        this.fund_image = "assets/butler_logo.png";
    }
    onEditAccount() {
        const dialogRef = this.dialog.open(AccountUpdateComponent, {
            width: '355px',
            position: { right: '24px', top: '76px' },
            panelClass: 'send-success-container'
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
};
AccountComponent = __decorate([
    Component({
        selector: 'app-account',
        templateUrl: './account.component.html',
        styleUrls: ['./account.component.css']
    })
], AccountComponent);
export { AccountComponent };
//# sourceMappingURL=account.component.js.map