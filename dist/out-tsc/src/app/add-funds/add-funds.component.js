import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AddFundsComponent = class AddFundsComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
    onClose() {
        this.dialogRef.close();
    }
};
AddFundsComponent = __decorate([
    Component({
        selector: 'app-add-funds',
        templateUrl: './add-funds.component.html',
        styleUrls: ['./add-funds.component.css']
    })
], AddFundsComponent);
export { AddFundsComponent };
//# sourceMappingURL=add-funds.component.js.map