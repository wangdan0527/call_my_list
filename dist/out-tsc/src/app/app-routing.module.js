import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SoundsComponent } from "./sounds/sounds.component";
import { MyCampaignsComponent } from './my-campaigns/my-campaigns.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { SupportComponent } from './support/support.component';
import { AccountComponent } from './account/account.component';
import { MessageCenterComponent } from './message-center/message-center.component';
import { SettingsComponent } from './settings/settings.component';
const routes = [
    { path: '', component: AppComponent, children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'sounds', component: SoundsComponent },
            { path: 'my-campaigns', component: MyCampaignsComponent },
            { path: 'contact-list', component: ContactListComponent },
            { path: 'support', component: SupportComponent },
            { path: 'account', component: AccountComponent },
            { path: 'message-center', component: MessageCenterComponent },
            { path: 'settings', component: SettingsComponent }
        ] },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map