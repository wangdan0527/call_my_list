import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
let CoreModule = class CoreModule {
};
CoreModule = __decorate([
    NgModule({
        declarations: [HeaderComponent, SidebarComponent],
        imports: [
            CommonModule,
            RouterModule
        ],
        exports: [HeaderComponent, SidebarComponent]
    })
], CoreModule);
export { CoreModule };
//# sourceMappingURL=core.module.js.map