import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { AddFundsComponent } from '../../add-funds/add-funds.component';
let HeaderComponent = class HeaderComponent {
    constructor(dialog) {
        this.dialog = dialog;
    }
    ngOnInit() {
        this.balance = 30;
    }
    toggleSidebar() {
        const dom = document.querySelector('body');
        dom.classList.toggle('push-right');
    }
    onAddFunds() {
        const dialogRef = this.dialog.open(AddFundsComponent, {
            width: '966px',
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
};
HeaderComponent = __decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.css']
    })
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map