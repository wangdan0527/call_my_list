import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SidebarComponent = class SidebarComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    onNavigate() {
        const dom = document.querySelector('body');
        dom.classList.add('push-right');
    }
};
SidebarComponent = __decorate([
    Component({
        selector: 'app-sidebar',
        templateUrl: './sidebar.component.html',
        styleUrls: ['./sidebar.component.css']
    })
], SidebarComponent);
export { SidebarComponent };
//# sourceMappingURL=sidebar.component.js.map