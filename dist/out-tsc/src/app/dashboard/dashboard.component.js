import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
let DashboardComponent = class DashboardComponent {
    constructor() {
        this.nStep = 0;
        this.doughnutChartLabels = [];
        this.doughnutChartType = 'doughnut';
        this.doughnutOptions = {
            rotation: Math.PI,
            cutoutPercentage: 60,
            circumference: Math.PI,
            responsive: true
        };
        this.textCampaigns = [
            {
                name: "North Central",
                dateSent: "1/1/2018",
                completed: true
            },
            {
                name: "North Central",
                dateSent: "1/1/2018",
                completed: true
            },
            {
                name: "North Central",
                dateSent: "1/1/2018",
                completed: true
            },
            {
                name: "North Central",
                dateSent: "1/1/2018",
                completed: false
            }
        ];
        this.callCampaigns = [
            {
                name: "North Central",
                dateSent: "1/1/2018",
                totalContacts: 13384,
                totalDialed: 13212,
                live: 2031,
                voicemail: 13384,
                transfer: 13384,
                listeningDuration: 38
            },
            {
                name: "North Central",
                dateSent: "1/1/2018",
                totalContacts: 232,
                totalDialed: 221,
                live: 5,
                voicemail: 232,
                transfer: 232,
                listeningDuration: 76
            },
            {
                name: "North Central",
                dateSent: "1/1/2018",
                totalContacts: 84485,
                totalDialed: 81215,
                live: 23487,
                voicemail: 84485,
                transfer: 84485,
                listeningDuration: 51
            },
            {
                name: "North Central",
                dateSent: "1/1/2018",
                totalContacts: 34373,
                totalDialed: 33703,
                live: 10038,
                voicemail: 34373,
                transfer: 34373,
                listeningDuration: 89
            }
        ];
        this.mySounds = [
            { name: "North Central", lastUsed: "1/1/2018" },
            { name: "North Central", lastUsed: "1/1/2018" },
            { name: "North Central", lastUsed: "1/1/2018" },
            { name: "North Central", lastUsed: "1/1/2018" }
        ];
        this.myContactList = [
            { name: "North Central", total: 100 },
            { name: "North Central", total: 100 },
            { name: "North Central", total: 100 },
            { name: "North Central", total: 100 }
        ];
    }
    ngOnInit() {
        let context = this.el.nativeElement.getContext("2d");
        let gradient = context.createLinearGradient(0, 0, 150, 0);
        gradient.addColorStop(0, '#be5112');
        gradient.addColorStop(1, '#f26d24');
        this.doughnutChartData = [
            {
                data: [70, 30],
                backgroundColor: [
                    gradient,
                    '#23242511'
                ]
            }
        ];
    }
    next() {
        if (this.nStep > 2) {
            return;
        }
        this.nStep++;
    }
};
__decorate([
    ViewChild('chartContainer', { static: true })
], DashboardComponent.prototype, "el", void 0);
DashboardComponent = __decorate([
    Component({
        selector: 'app-dashboard',
        templateUrl: './dashboard.component.html',
        styleUrls: ['./dashboard.component.css']
    })
], DashboardComponent);
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map