import { __decorate } from "tslib";
import { Component } from '@angular/core';
let LoadingDataComponent = class LoadingDataComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
LoadingDataComponent = __decorate([
    Component({
        selector: 'app-loading-data',
        templateUrl: './loading-data.component.html',
        styleUrls: ['./loading-data.component.css']
    })
], LoadingDataComponent);
export { LoadingDataComponent };
//# sourceMappingURL=loading-data.component.js.map