import { __decorate } from "tslib";
import { Component } from '@angular/core';
let LoadingFinishedComponent = class LoadingFinishedComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
LoadingFinishedComponent = __decorate([
    Component({
        selector: 'app-loading-finished',
        templateUrl: './loading-finished.component.html',
        styleUrls: ['./loading-finished.component.css']
    })
], LoadingFinishedComponent);
export { LoadingFinishedComponent };
//# sourceMappingURL=loading-finished.component.js.map