import { __decorate } from "tslib";
import { Component } from '@angular/core';
let NewMessageComponent = class NewMessageComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
NewMessageComponent = __decorate([
    Component({
        selector: 'app-new-message',
        templateUrl: './new-message.component.html',
        styleUrls: ['./new-message.component.css']
    })
], NewMessageComponent);
export { NewMessageComponent };
//# sourceMappingURL=new-message.component.js.map