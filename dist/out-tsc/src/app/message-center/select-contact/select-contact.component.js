import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SelectContactComponent = class SelectContactComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
        this.contacts_list = [
            {
                name: "Pavel Korchakin",
                company: "Company Name",
                phone: "(398) 343-3948",
                last_activity: "1/1/2019"
            },
            {
                name: "Pavel Korchakin",
                company: "Company Name",
                phone: "(398) 343-3948",
                last_activity: "1/1/2019"
            },
            {
                name: "Pavel Korchakin",
                company: "Company Name",
                phone: "(398) 343-3948",
                last_activity: "1/1/2019"
            },
        ];
    }
    ngOnInit() {
    }
    onAddContact() {
        this.dialogRef.close();
    }
};
SelectContactComponent = __decorate([
    Component({
        selector: 'app-select-contact',
        templateUrl: './select-contact.component.html',
        styleUrls: ['./select-contact.component.css']
    })
], SelectContactComponent);
export { SelectContactComponent };
//# sourceMappingURL=select-contact.component.js.map