import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { UpdateCampaignComponent } from './update-campaign/update-campaign.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
let MyCampaignsComponent = class MyCampaignsComponent {
    constructor(dialog) {
        this.dialog = dialog;
        this.COMPAIGN_TYPE_TEXT = 1;
        this.COMPAIGN_TYPE_CALL = 2;
        this.SCHEDULING_NOW = 1;
        this.SCHEDULING_FUTURE = 2;
        this.createStage = false;
        this.campaignType = 0;
        this.nStep = 0;
        this.campaign_process = ["Campaign Type", "Campaign Name", "Caller ID", "Audio Files", "Contact List", "Campaign Settings", "Scheduling"];
        this.transfer = false;
        this.useDefaultAudio = false;
        this.doNotCall = false;
        this.uploadContactList = false;
        this.settingsCount = 0;
        this.schedulingType = 0;
        this.bCompleted = false;
        this.callCampaings = [];
        this.callCampaigns_data = [
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "unchecked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "scheduled"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            },
            {
                name: "North Central",
                cpm: 2,
                total_contacts: 123,
                total_dialed: 123,
                live: 123,
                voicemail: 123,
                transfer: 123,
                dnc: 123,
                duration: 123,
                state: "checked"
            }
        ];
        this.hasError = (controlName, errorName) => {
            return this.counterForm.controls[controlName].hasError(errorName);
        };
    }
    ngOnInit() {
        this.counterForm = new FormGroup({
            counter: new FormControl('', [Validators.required, Validators.max(50)])
        });
    }
    onCreateCampaign() {
        this.createStage = true;
        // this.callCampaings = this.callCampaigns_data
    }
    onUpdate(event) {
        const dialogRef = this.dialog.open(UpdateCampaignComponent, {
            width: '370px',
            height: '76px',
            position: { top: event.clientY - 38 + 'px', left: Math.min(event.clientX + 50, window.innerWidth - 370) + 'px' },
            panelClass: 'update-campaign',
            backdropClass: 'update-background'
            // data: {name: this.name, animal: this.animal}
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
    onSelectType(campaignType) {
        this.campaignType = campaignType;
    }
    onSelectScheduling(schedulingType) {
        this.schedulingType = schedulingType;
    }
    onContinue() {
        if (this.nStep > 5) {
            this.callCampaings = this.callCampaigns_data;
            this.createStage = false;
            return;
        }
        this.nStep++;
    }
    onStep(step) {
        this.nStep = step;
    }
    onComplete() {
        this.bCompleted = true;
    }
    onToggleTransfer(event) {
        this.transfer = event.checked;
    }
    onDoNotCall(event) {
        this.doNotCall = event.checked;
    }
    onUseDefaultAudio(event) {
        this.useDefaultAudio = event.checked;
    }
    onUploadContactList() {
        this.uploadContactList = true;
    }
};
MyCampaignsComponent = __decorate([
    Component({
        selector: 'app-my-campaigns',
        templateUrl: './my-campaigns.component.html',
        styleUrls: ['./my-campaigns.component.css']
    })
], MyCampaignsComponent);
export { MyCampaignsComponent };
//# sourceMappingURL=my-campaigns.component.js.map