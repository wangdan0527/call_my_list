import { __decorate } from "tslib";
import { Component } from '@angular/core';
let UpdateCampaignComponent = class UpdateCampaignComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
    onUpdate() {
        this.dialogRef.close();
    }
};
UpdateCampaignComponent = __decorate([
    Component({
        selector: 'app-update-campaign',
        templateUrl: './update-campaign.component.html',
        styleUrls: ['./update-campaign.component.css']
    })
], UpdateCampaignComponent);
export { UpdateCampaignComponent };
//# sourceMappingURL=update-campaign.component.js.map