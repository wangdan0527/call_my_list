import { __decorate } from "tslib";
import { Component } from '@angular/core';
let TextSpeechComponent = class TextSpeechComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
TextSpeechComponent = __decorate([
    Component({
        selector: 'app-text-speech',
        templateUrl: './text-speech.component.html',
        styleUrls: ['./text-speech.component.css']
    })
], TextSpeechComponent);
export { TextSpeechComponent };
//# sourceMappingURL=text-speech.component.js.map