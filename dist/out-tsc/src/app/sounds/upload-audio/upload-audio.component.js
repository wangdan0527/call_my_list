import { __decorate } from "tslib";
import { Component } from '@angular/core';
let UploadAudioComponent = class UploadAudioComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
UploadAudioComponent = __decorate([
    Component({
        selector: 'app-upload-audio',
        templateUrl: './upload-audio.component.html',
        styleUrls: ['./upload-audio.component.css']
    })
], UploadAudioComponent);
export { UploadAudioComponent };
//# sourceMappingURL=upload-audio.component.js.map