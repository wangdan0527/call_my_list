import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SendSuccessComponent = class SendSuccessComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
SendSuccessComponent = __decorate([
    Component({
        selector: 'app-send-success',
        templateUrl: './send-success.component.html',
        styleUrls: ['./send-success.component.css']
    })
], SendSuccessComponent);
export { SendSuccessComponent };
//# sourceMappingURL=send-success.component.js.map