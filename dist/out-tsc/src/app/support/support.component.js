import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { SendSuccessComponent } from './send-success/send-success.component';
let SupportComponent = class SupportComponent {
    constructor(dialog) {
        this.dialog = dialog;
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.phone = "";
        this.message = "";
    }
    ngOnInit() {
    }
    onSendMessage() {
        const dialogRef = this.dialog.open(SendSuccessComponent, {
            width: '331px',
            position: { right: '24px', top: '76px' },
            panelClass: 'send-success-container'
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
        this.firstName = "";
        this.firstName = "";
        this.firstName = "";
        this.firstName = "";
        this.firstName = "";
    }
};
SupportComponent = __decorate([
    Component({
        selector: 'app-support',
        templateUrl: './support.component.html',
        styleUrls: ['./support.component.css']
    })
], SupportComponent);
export { SupportComponent };
//# sourceMappingURL=support.component.js.map