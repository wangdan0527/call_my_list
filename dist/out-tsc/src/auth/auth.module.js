import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthComponent } from './auth/auth.component';
import { MatFormFieldModule } from "@angular/material";
import { MatInputModule } from "@angular/material";
import { ResetPasswordComponent } from './reset-password/reset-password.component';
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    NgModule({
        declarations: [LoginComponent, SignupComponent, AuthComponent, ResetPasswordComponent],
        imports: [
            CommonModule,
            AuthRoutingModule,
            MatFormFieldModule,
            MatInputModule
        ]
    })
], AuthModule);
export { AuthModule };
//# sourceMappingURL=auth.module.js.map