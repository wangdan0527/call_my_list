import { __decorate } from "tslib";
import { Component } from '@angular/core';
let LoginComponent = class LoginComponent {
    constructor() {
        this.bTogglePassword = false;
        this.bLoginFailed = false;
    }
    ngOnInit() {
    }
    togglePassword() {
        this.bTogglePassword = !this.bTogglePassword;
    }
    login() {
        this.bLoginFailed = true;
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map