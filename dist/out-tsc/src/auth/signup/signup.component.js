import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SignupComponent = class SignupComponent {
    constructor(router) {
        this.router = router;
        this.bTogglePassword = false;
    }
    ngOnInit() {
    }
    togglePassword() {
        this.bTogglePassword = !this.bTogglePassword;
    }
    signup() {
        this.router.navigateByUrl('auth/login');
    }
};
SignupComponent = __decorate([
    Component({
        selector: 'app-signup',
        templateUrl: './signup.component.html',
        styleUrls: ['./signup.component.css']
    })
], SignupComponent);
export { SignupComponent };
//# sourceMappingURL=signup.component.js.map