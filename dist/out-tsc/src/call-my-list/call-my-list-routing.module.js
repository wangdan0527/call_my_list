import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
const routes = [
    { path: 'auth', loadChildren: '../auth/auth.module#AuthModule' },
    { path: 'app', loadChildren: '../app/app.module#AppModule' }
];
let CallMyListRoutingModule = class CallMyListRoutingModule {
};
CallMyListRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes, { useHash: true })],
        exports: [RouterModule]
    })
], CallMyListRoutingModule);
export { CallMyListRoutingModule };
//# sourceMappingURL=call-my-list-routing.module.js.map