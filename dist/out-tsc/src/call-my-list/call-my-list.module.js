import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CallMyListRoutingModule } from './call-my-list-routing.module';
import { RootComponentComponent } from './root-component/root-component.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
let CallMyListModule = class CallMyListModule {
};
CallMyListModule = __decorate([
    NgModule({
        declarations: [RootComponentComponent],
        imports: [
            BrowserModule,
            BrowserAnimationsModule,
            CallMyListRoutingModule
        ],
        providers: [],
        bootstrap: [RootComponentComponent]
    })
], CallMyListModule);
export { CallMyListModule };
//# sourceMappingURL=call-my-list.module.js.map