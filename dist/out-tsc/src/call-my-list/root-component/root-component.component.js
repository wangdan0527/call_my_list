import { __decorate } from "tslib";
import { Component } from '@angular/core';
let RootComponentComponent = class RootComponentComponent {
    constructor() { }
    ngOnInit() {
    }
};
RootComponentComponent = __decorate([
    Component({
        selector: 'app-root-component',
        templateUrl: './root-component.component.html',
        styleUrls: ['./root-component.component.css']
    })
], RootComponentComponent);
export { RootComponentComponent };
//# sourceMappingURL=root-component.component.js.map